<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $user_id
 * @property int $meal_id
 * @property int $status
 * @property int $bread_id
 * @property string $size
 * @property int $should_baked
 * @property int $taste_id
 * @property int $sauce_id
 * @property string $extras
 * @property string $created_at
 * @property int $rate
 *
 * @property Bread $bread
 * @property Meal $meal
 * @property Sauce $sauce
 * @property Taste $taste
 * @property User $user
 * @property OrderVegetable[] $orderVegetables
 * @property Vegetable[] $vegetables
 */
class Order extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'meal_id', 'size', 'should_baked'], 'required'],
            [['user_id', 'meal_id', 'status', 'bread_id', 'should_baked', 'taste_id', 'sauce_id'], 'integer'],
            [['size', 'extras'], 'string'],
            [['created_at'], 'safe'],
            [
                ['bread_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Bread::class,
                'targetAttribute' => ['bread_id' => 'id'],
            ],
            [
                ['meal_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Meal::class,
                'targetAttribute' => ['meal_id' => 'id'],
            ],
            [
                ['sauce_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sauce::class,
                'targetAttribute' => ['sauce_id' => 'id'],
            ],
            [
                ['taste_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Taste::class,
                'targetAttribute' => ['taste_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;
        $data = $data[$scope];

        if (empty($this->user_id)) {
            $this->user_id = Yii::$app->user->getId();
        }

        if ($data['should_baked'] === null) {
            $data['should_baked'] = 0;
        }

        return parent::load($data, '');
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'meal_id' => 'Meal ID',
            'status' => 'Status',
            'bread_id' => 'Bread ID',
            'size' => 'Size',
            'should_baked' => 'Should Baked',
            'taste_id' => 'Taste ID',
            'sauce_id' => 'Sauce ID',
            'extras' => 'Extras',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBread()
    {
        return $this->hasOne(Bread::class, ['id' => 'bread_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeal()
    {
        return $this->hasOne(Meal::class, ['id' => 'meal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSauce()
    {
        return $this->hasOne(Sauce::class, ['id' => 'sauce_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaste()
    {
        return $this->hasOne(Taste::class, ['id' => 'taste_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderVegetables()
    {
        return $this->hasMany(OrderVegetable::class, ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVegetables()
    {
        return $this->hasMany(Vegetable::class, ['id' => 'vegetable_id'])->viaTable('order_vegetable',
            ['order_id' => 'id']);
    }
}
