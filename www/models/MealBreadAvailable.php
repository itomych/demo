<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meal_bread_available".
 *
 * @property int $meal_id
 * @property int $bread_id
 *
 * @property Bread $bread
 * @property Meal $meal
 */
class MealBreadAvailable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meal_bread_available';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meal_id', 'bread_id'], 'required'],
            [['meal_id', 'bread_id'], 'integer'],
            [['meal_id', 'bread_id'], 'unique', 'targetAttribute' => ['meal_id', 'bread_id']],
            [['bread_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bread::class, 'targetAttribute' => ['bread_id' => 'id']],
            [['meal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meal::class, 'targetAttribute' => ['meal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'meal_id' => 'Meal ID',
            'bread_id' => 'Bread ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBread()
    {
        return $this->hasOne(Bread::class, ['id' => 'bread_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeal()
    {
        return $this->hasOne(Meal::class, ['id' => 'meal_id']);
    }
}
