<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vegetable".
 *
 * @property int $id
 * @property string $name
 *
 * @property MealVegetableAvailable[] $mealVegetableAvailables
 * @property Meal[] $meals
 */
class Vegetable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vegetable';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMealVegetableAvailables()
    {
        return $this->hasMany(MealVegetableAvailable::class, ['vegetable_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeals()
    {
        return $this->hasMany(Meal::class, ['id' => 'meal_id'])->viaTable('meal_vegetable_available', ['vegetable_id' => 'id']);
    }
}
