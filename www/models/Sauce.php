<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sauce".
 *
 * @property int $id
 * @property string $name
 *
 * @property MealSauceAvailable[] $mealSauceAvailables
 * @property Meal[] $meals
 * @property Order[] $orders
 */
class Sauce extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sauce';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMealSauceAvailables()
    {
        return $this->hasMany(MealSauceAvailable::class, ['sauce_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeals()
    {
        return $this->hasMany(Meal::class, ['id' => 'meal_id'])->viaTable('meal_sauce_available', ['sauce_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['sauce_id' => 'id']);
    }
}
