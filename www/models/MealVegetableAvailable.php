<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meal_vegetable_available".
 *
 * @property int $meal_id
 * @property int $vegetable_id
 *
 * @property Meal $meal
 * @property Vegetable $vegetable
 */
class MealVegetableAvailable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meal_vegetable_available';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meal_id', 'vegetable_id'], 'required'],
            [['meal_id', 'vegetable_id'], 'integer'],
            [['meal_id', 'vegetable_id'], 'unique', 'targetAttribute' => ['meal_id', 'vegetable_id']],
            [['meal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meal::class, 'targetAttribute' => ['meal_id' => 'id']],
            [['vegetable_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vegetable::class, 'targetAttribute' => ['vegetable_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'meal_id' => 'Meal ID',
            'vegetable_id' => 'Vegetable ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeal()
    {
        return $this->hasOne(Meal::class, ['id' => 'meal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVegetable()
    {
        return $this->hasOne(Vegetable::class, ['id' => 'vegetable_id']);
    }
}
