<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meal_taste_available".
 *
 * @property int $meal_id
 * @property int $taste_id
 *
 * @property Taste $taste
 * @property Meal $meal
 */
class MealTasteAvailable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meal_taste_available';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meal_id', 'taste_id'], 'required'],
            [['meal_id', 'taste_id'], 'integer'],
            [['meal_id', 'taste_id'], 'unique', 'targetAttribute' => ['meal_id', 'taste_id']],
            [['taste_id'], 'exist', 'skipOnError' => true, 'targetClass' => Taste::class, 'targetAttribute' => ['taste_id' => 'id']],
            [['meal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meal::class, 'targetAttribute' => ['meal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'meal_id' => 'Meal ID',
            'taste_id' => 'Taste ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaste()
    {
        return $this->hasOne(Taste::class, ['id' => 'taste_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeal()
    {
        return $this->hasOne(Meal::class, ['id' => 'meal_id']);
    }
}
