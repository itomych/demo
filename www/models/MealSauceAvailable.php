<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meal_sauce_available".
 *
 * @property int $meal_id
 * @property int $sauce_id
 *
 * @property Meal $meal
 * @property Sauce $sauce
 */
class MealSauceAvailable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meal_sauce_available';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meal_id', 'sauce_id'], 'required'],
            [['meal_id', 'sauce_id'], 'integer'],
            [['meal_id', 'sauce_id'], 'unique', 'targetAttribute' => ['meal_id', 'sauce_id']],
            [['meal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meal::class, 'targetAttribute' => ['meal_id' => 'id']],
            [['sauce_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sauce::class, 'targetAttribute' => ['sauce_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'meal_id' => 'Meal ID',
            'sauce_id' => 'Sauce ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeal()
    {
        return $this->hasOne(Meal::class, ['id' => 'meal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSauce()
    {
        return $this->hasOne(Sauce::class, ['id' => 'sauce_id']);
    }
}
