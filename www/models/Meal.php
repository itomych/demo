<?php

namespace app\models;

/**
 * This is the model class for table "meal".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property int $status
 * @property int $available_bake
 * @property array $available_extras
 * @property array $available_sizes
 * @property string $access_code
 * @property string $available_sizes_string
 * @property string $available_extras_string
 *
 * @property MealBreadAvailable[] $mealBreadAvailables
 * @property Bread[] $breads
 * @property MealSauceAvailable[] $mealSauceAvailables
 * @property Sauce[] $sauces
 * @property MealTasteAvailable[] $mealTasteAvailables
 * @property Taste[] $tastes
 * @property MealVegetableAvailable[] $mealVegetableAvailables
 * @property Vegetable[] $vegetables
 * @property Order[] $orders
 */
class Meal extends \yii\db\ActiveRecord
{
    public $available_sizes_string;
    public $available_extras_string;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status', 'available_bake', 'available_extras', 'available_sizes'], 'required'],
            [['created_at'], 'safe'],
            [['available_bake'], 'integer'],
            [['status'], 'integer'],
            ['status', 'singleOpened'],
            [['available_extras', 'available_sizes'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function singleOpened($attribute, $params, $validator): void
    {
        if ($this->$attribute) {
            $openedMeal = $this::find()->where([$attribute => 1])->select(['id', $attribute])->one();
            if (isset($openedMeal) && $openedMeal->id !== $this->id) {
                $this->addError($attribute, 'Only one meal can be “open” at a single time');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'status' => 'Status',
            'available_bake' => 'Available Bake',
            'available_extras' => 'Available Extras',
            'available_sizes' => 'Available Sizes',
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;
        $data = $data[$scope];

        if (is_array($data['available_extras'])) {
            $data['available_extras'] = join(',', $data['available_extras']);
        }
        if (is_array($data['available_sizes'])) {
            $data['available_sizes'] = join(',', $data['available_sizes']);
        }

        return parent::load($data, '');
    }

    /**
     * @param string $name
     * @param bool $delete +
     */
    public function unlinkAll($name, $delete = true)
    {
        parent::unlinkAll($name, $delete);
    }

    public function beforeSave($insert)
    {
        if (empty($this->access_code)) {
            $this->access_code = uniqid();
        }

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->available_extras_string = $this->available_extras;
        $this->available_sizes_string = $this->available_sizes;
        $this->available_sizes = explode(',', $this->available_sizes_string);
        $this->available_extras = explode(',', $this->available_extras_string);

        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMealBreadAvailables()
    {
        return $this->hasMany(MealBreadAvailable::class, ['meal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getBreads()
    {
        return $this->hasMany(Bread::class, ['id' => 'bread_id'])->viaTable('meal_bread_available',
            ['meal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMealSauceAvailables()
    {
        return $this->hasMany(MealSauceAvailable::class, ['meal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getSauces()
    {
        return $this->hasMany(Sauce::class, ['id' => 'sauce_id'])->viaTable('meal_sauce_available',
            ['meal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMealTasteAvailables()
    {
        return $this->hasMany(MealTasteAvailable::class, ['meal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getTastes()
    {
        return $this->hasMany(Taste::class, ['id' => 'taste_id'])->viaTable('meal_taste_available',
            ['meal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMealVegetableAvailables()
    {
        return $this->hasMany(MealVegetableAvailable::class, ['meal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getVegetables()
    {
        return $this->hasMany(Vegetable::class, ['id' => 'vegetable_id'])->viaTable('meal_vegetable_available',
            ['meal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['meal_id' => 'id']);
    }
}
