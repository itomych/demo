<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_vegetable".
 *
 * @property int $order_id
 * @property int $vegetable_id
 *
 * @property Order $order
 * @property Vegetable $vegetable
 */
class OrderVegetable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_vegetable';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'vegetable_id'], 'required'],
            [['order_id', 'vegetable_id'], 'integer'],
            [['order_id', 'vegetable_id'], 'unique', 'targetAttribute' => ['order_id', 'vegetable_id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['order_id' => 'id']],
            [['vegetable_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vegetable::class, 'targetAttribute' => ['vegetable_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'vegetable_id' => 'Vegetable ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVegetable()
    {
        return $this->hasOne(Vegetable::class, ['id' => 'vegetable_id']);
    }
}
