<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "taste".
 *
 * @property int $id
 * @property string $name
 *
 * @property MealTasteAvailable[] $mealTasteAvailables
 * @property Meal[] $meals
 * @property Order[] $orders
 */
class Taste extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'taste';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMealTasteAvailables()
    {
        return $this->hasMany(MealTasteAvailable::class, ['taste_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeals()
    {
        return $this->hasMany(Meal::class, ['id' => 'meal_id'])->viaTable('meal_taste_available', ['taste_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['taste_id' => 'id']);
    }
}
