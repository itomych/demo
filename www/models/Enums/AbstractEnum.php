<?php

namespace app\models\Enums;


abstract class AbstractEnum
{
    abstract public static function getList(): array;

    /**
     * @param array $values
     * @return array
     */
    public static function getListFromValues(array $values): array
    {
        $values = array_flip($values);
        return array_intersect_key(static::getList(), $values);
    }


    /**
     * @param mixed $value
     * @return string
     */
    public static function getName($value): string
    {
        return static::getList()[$value];
    }
}