<?php

namespace app\models\Enums;

class Sizes extends AbstractEnum
{
    const CM15 = '15';
    const CM30 = '30';

    /**
     * @return array
     */
    public static function getList(): array
    {
        return [
            static::CM15 => '15cm',
            static::CM30 => '30cm',
        ];
    }

}