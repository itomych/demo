<?php

namespace app\models\Enums;

class OpenedClosed extends AbstractEnum
{
    const OPENED = 1;
    const CLOSED = 0;

    /**
     * @return array
     */
    public static function getList(): array
    {
        return [
            static::OPENED => 'Opened',
            static::CLOSED => 'Closed',
        ];
    }
}