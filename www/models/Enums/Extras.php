<?php

namespace app\models\Enums;

class Extras extends AbstractEnum
{
    const BACON = 'bacon';
    const MEAT = 'meat';
    const CHEESE = 'cheese';

    /**
     * @return array
     */
    public static function getList(): array
    {
        return [
            static::BACON => 'Bacon',
            static::MEAT => 'Meat',
            static::CHEESE => 'Cheese',
        ];
    }

    /**
     * @param array $values
     * @return array
     */
    public static function getListFromValues(array $values): array
    {
        $values = array_flip($values);
        return array_intersect_key(static::getList(), $values);
    }
}