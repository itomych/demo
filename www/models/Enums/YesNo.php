<?php

namespace app\models\Enums;

class YesNo extends AbstractEnum
{
    const YES = 1;
    const NO = 0;

    /**
     * @return array
     */
    public static function getList(): array
    {
        return [
            static::YES => 'Yes',
            static::NO => 'No',
        ];
    }
}