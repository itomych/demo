<?php
return [
    'createOrder' => [
        'type' => 2,
    ],
    'adminChanges' => [
        'type' => 2,
    ],
    'author' => [
        'type' => 1,
        'children' => [
            'createOrder',
            'updateOwnOrder',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'adminChanges',
        ],
    ],
    'updateOwnOrder' => [
        'type' => 2,
        'ruleName' => 'isAuthor',
    ],
];
