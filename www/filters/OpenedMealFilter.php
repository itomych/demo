<?php

namespace app\filters;

use app\models\Meal;
use Yii;
use yii\base\ActionEvent;
use yii\base\ActionFilter;

class OpenedMealFilter extends ActionFilter
{
    public $returnUrl = '/';

    /**
     * @param ActionEvent $event
     * @return bool
     */
    public function beforeAction($event)
    {
        $meal = Meal::find()->where(['status' => 1])->one();
        if (empty($meal)) {
            Yii::$app->session->setFlash('error', 'Not meal opened now!');
            Yii::$app->getResponse()->redirect(Yii::$app->getUser()->getReturnUrl($this->returnUrl));
            return false;
        }

        return true;
    }
}
