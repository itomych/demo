<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=subwaymysql;dbname=subway',
    'username' => 'subway',
    'password' => 'subway',
    'charset' => 'utf8mb4',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
