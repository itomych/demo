<?php

namespace app\controllers;

use app\models\Bread;
use app\models\Enums\Extras;
use app\models\Enums\OpenedClosed;
use app\models\Enums\Sizes;
use app\models\Enums\YesNo;
use app\models\Meal;
use app\models\Sauce;
use app\models\Taste;
use app\models\Vegetable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MealController implements the CRUD actions for Meal model.
 */
class MealController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete', 'update', 'create'],
                        'roles' => ['adminChanges'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Meal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Meal::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Meal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $isGuest = Yii::$app->user->getIsGuest();
        $accessCode = Yii::$app->request->get('code');

        if ($isGuest && $accessCode !== $model->access_code) {
            $this->redirect('/site/login');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Meal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new Meal();
        $postData = Yii::$app->request->post();

        if ($model->load($postData) && $model->save()) {

            $breads = Bread::find()->where(['in', 'id', $postData['Meal']['breads']])->all();
            foreach ($breads as $bread) {
                $model->link('breads', $bread);
            }
            $tastes = Taste::find()->where(['in', 'id', $postData['Meal']['tastes']])->all();
            foreach ($tastes as $taste) {
                $model->link('tastes', $taste);
            }
            $sauces = Sauce::find()->where(['in', 'id', $postData['Meal']['sauces']])->all();
            foreach ($sauces as $sauce) {
                $model->link('sauces', $sauce);
            }
            $vegetables = Vegetable::find()->where(['in', 'id', $postData['Meal']['vegetables']])->all();
            foreach ($vegetables as $vegetable) {
                $model->link('vegetables', $vegetable);
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $breads = array_column(Bread::find()->all(), 'name', 'id');
        $tastes = array_column(Taste::find()->all(), 'name', 'id');
        $sauces = array_column(Sauce::find()->all(), 'name', 'id');
        $vegetables = array_column(Vegetable::find()->all(), 'name', 'id');
        $extras = Extras::getList();
        $sizes = Sizes::getList();
        $openClose = OpenedClosed::getList();
        $yesNo = YesNo::getList();

        return $this->render('create',
            compact('breads', 'tastes', 'vegetables', 'sauces', 'model', 'extras', 'sizes', 'openClose', 'yesNo')
        );
    }

    /**
     * Updates an existing Meal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $postData = Yii::$app->request->post();

        if ($model->load($postData) && $model->save()) {

            $model->unlinkAll('breads');
            $model->unlinkAll('tastes');
            $model->unlinkAll('sauces');
            $model->unlinkAll('vegetables');

            $breads = Bread::find()->where(['in', 'id', $postData['Meal']['breads']])->all();
            foreach ($breads as $bread) {
                $model->link('breads', $bread);
            }
            $tastes = Taste::find()->where(['in', 'id', $postData['Meal']['tastes']])->all();
            foreach ($tastes as $taste) {
                $model->link('tastes', $taste);
            }
            $sauces = Sauce::find()->where(['in', 'id', $postData['Meal']['sauces']])->all();
            foreach ($sauces as $sauce) {
                $model->link('sauces', $sauce);
            }
            $vegetables = Vegetable::find()->where(['in', 'id', $postData['Meal']['vegetables']])->all();
            foreach ($vegetables as $vegetable) {
                $model->link('vegetables', $vegetable);
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $breads = array_column(Bread::find()->all(), 'name', 'id');
        $tastes = array_column(Taste::find()->all(), 'name', 'id');
        $sauces = array_column(Sauce::find()->all(), 'name', 'id');
        $vegetables = array_column(Vegetable::find()->all(), 'name', 'id');
        $extras = Extras::getList();
        $sizes = Sizes::getList();
        $openClose = OpenedClosed::getList();
        $yesNo = YesNo::getList();

        return $this->render('update',
            compact('breads', 'tastes', 'vegetables', 'sauces', 'model', 'extras', 'sizes', 'openClose', 'yesNo')
        );
    }

    /**
     * Deletes an existing Meal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Meal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Meal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
