<?php

namespace app\controllers;

use app\filters\OpenedMealFilter;
use app\models\Meal;
use app\models\Order;
use app\services\OrderService;
use Yii;
use yii\base\Module;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @var OrderService
     */
    private $orderService;

    public function __construct(string $id, Module $module, array $config = [], OrderService $orderService)
    {
        parent::__construct($id, $module, $config);
        $this->orderService = $orderService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            [
                'class' => OpenedMealFilter::class,
                'only' => ['create', 'update'],
                'returnUrl' => '/order',
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'create-page'],
                        'roles' => ['createOrder'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'update', 'view', 'update-page', 'rate'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete', 'open', 'close'],
                        'roles' => ['adminChanges'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Order::find();
        $authUser = Yii::$app->user;
        if (!$authUser->can('adminChanges')) {
            $query = $query->where(['user_id' => $authUser->getId()]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
        $order = $this->findModel($id);

        if (!$this->isHassAccessToOrder($order)) {
            throw new ForbiddenHttpException('You has no access for this order!');
        }

        return $this->render('view', [
            'model' => $order,
        ]);
    }

    protected function isHassAccessToOrder(Order $order)
    {
        return Yii::$app->user->can('updateOwnOrder', ['order' => $order]) ||
            Yii::$app->authManager->getAssignment('admin', Yii::$app->user->getId());
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $model = new Order();

        /**
         * @var $meal Meal
         */
        $meal = Meal::find()->where(['status' => 1])->one();

        $postData = Yii::$app->request->post();
        $postData['Order']['meal_id'] = $meal->id;
        $postData['Order']['status'] = 1;

        $isLoadModel = $model->load($postData);
        if ($isLoadModel && $model->save()) {
            $vegetables = $postData['Order']['vegetables'];
            $this->orderService->linkVegetables($model, $vegetables);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->showErrors($model);
    }

    /**
     * @param null $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreatePage($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
            if (!$this->isHassAccessToOrder($model)) {
                throw new ForbiddenHttpException('You has no access for copied order!');
            }
        } else {
            $model = new Order();
        }

        /**
         * @var $meal Meal
         */
        $meal = Meal::find()->where(['status' => 1])->one();

        $availableChoices = $this->orderService->getAvailableChoices($meal);
        $availableChoices['model'] = $model;

        return $this->render('create', $availableChoices);
    }

    /**
     * @param $id
     * @param $rate
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionRate($id, $rate)
    {
        if ($rate < 1 || $rate > 5) {
            throw new BadRequestHttpException();
        }

        $model = $this->findModel($id);
        if (!Yii::$app->user->can('updateOwnOrder', ['order' => $model])) {
            throw new ForbiddenHttpException('You has no access to rate the order');
        }

        $model->rate = $rate;
        $model->save();

        return $this->redirect('/order');
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionOpen($id)
    {
        $model = $this->findModel($id);
        $model->status = 1;
        $model->save();

        return $this->redirect('/order');
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionClose($id)
    {
        $model = $this->findModel($id);
        $model->status = 0;
        $model->save();

        return $this->redirect('/order');
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws ForbiddenHttpException
     */
    public function actionUpdatePage($id)
    {
        $model = $this->findModel($id);

        if (!$this->isHassAccessToOrder($model)) {
            throw new ForbiddenHttpException('You has no access for this order!');
        }

        if ($model->status === 0 && !Yii::$app->user->can('adminChanges')) {
            Yii::$app->session->addFlash('error', 'Sorry, this order has been closed!');
            return $this->redirect('/order');
        }

        /**
         * @var $meal Meal
         */
        $meal = $model->meal;
        $availableChoices = $this->orderService->getAvailableChoices($meal);
        $availableChoices['model'] = $model;

        return $this->render('update', $availableChoices);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidConfigException
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$this->isHassAccessToOrder($model)) {
            throw new ForbiddenHttpException('You has no access for this order!');
        }

        if ($model->status === 0 && !Yii::$app->user->can('adminChanges')) {
            Yii::$app->session->addFlash('error', 'Sorry, this order has been closed!');
            return $this->redirect('/order');
        }

        $postData = Yii::$app->request->post();

        $isLoadModel = $model->load($postData);
        if ($isLoadModel && $model->save()) {
            $model->unlinkAll('vegetables', true);

            $vegetables = $postData['Order']['vegetables'];
            $this->orderService->linkVegetables($model, $vegetables);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->showErrors($model);
    }

    /**
     * @param Order $model
     * @return \yii\web\Response
     */
    protected function showErrors(Order $model)
    {
        $errors = $model->getErrorSummary(true);
        foreach ($errors as $error) {
            Yii::$app->session->addFlash('error', $error);
        }

        return $this->redirect(['/order']);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
