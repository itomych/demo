<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Taste */

$this->title = 'Create Taste';
$this->params['breadcrumbs'][] = ['label' => 'Tastes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taste-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
