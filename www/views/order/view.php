<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-view">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
      <?= Html::a('Update', ['update-page', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Delete', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
              'confirm' => 'Are you sure you want to delete this item?',
              'method' => 'post',
          ],
      ]) ?>
  </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'label' => 'User name',
                'value' => function ($data) {
                    return $data->user->name;
                },
            ],
            [
                'attribute' => 'meal_id',
                'label' => 'Meal name',
                'value' => function ($data) {
                    return $data->meal->name;
                },
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function ($data) {
                    return \app\models\Enums\OpenedClosed::getName($data->status);
                },
            ],
            [
                'attribute' => 'bread',
                'label' => 'Bread',
                'value' => function ($data) {
                    return $data->bread->name;
                },
            ],
            [
                'attribute' => 'size',
                'label' => 'Size',
                'value' => function ($data) {
                    return \app\models\Enums\Sizes::getName($data->size);
                },
            ],
            [
                'attribute' => 'should_baked',
                'label' => 'Should baked',
                'value' => function ($data) {
                    return \app\models\Enums\YesNo::getName($data->should_baked);
                },
            ],
            [
                'attribute' => 'taste',
                'label' => 'Taste',
                'value' => function ($data) {
                    return $data->taste->name;
                },
            ],
            [
                'attribute' => 'sauce',
                'label' => 'Sauce',
                'value' => function ($data) {
                    return $data->sauce->name;
                },
            ],
            [
                'attribute' => 'extras',
                'label' => 'Extras',
                'value' => function ($data) {
                    return \app\models\Enums\Extras::getName($data->extras);
                },
            ],
            [
                'attribute' => 'vegetables',
                'label' => 'Vegetables',
                'value' => function (\app\models\Order $data) {
                    return \app\helpers\StringHelper::getJoinedNames($data->vegetables);
                },
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Created date',
            ],
        ],
    ]) ?>

</div>
