<?php

use dosamigos\multiselect\MultiSelect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">
    <?php if ($availableBreads) { ?>

        <?= $form->field($model, 'bread_id')->dropDownList($availableBreads) ?>

    <?php } ?>

    <?= $form->field($model, 'size')->dropDownList($availableSizes) ?>

    <?php if ($availableBake) { ?>

        <?= $form->field($model, 'should_baked')->dropDownList($yesNo) ?>

    <?php } ?>

    <?php if (!empty($availableTastes)) { ?>

        <?= $form->field($model, 'taste_id')->label('Taste')->dropDownList($availableTastes) ?>

    <?php } ?>

    <?php if (!empty($availableSauces)) { ?>

        <?= $form->field($model, 'sauce_id')->label('Sauce')->dropDownList($availableSauces) ?>

    <?php } ?>

    <?php if (!empty($availableExtras)) { ?>

        <?= $form->field($model, 'extras')->dropDownList($availableExtras) ?>

    <?php } ?>

    <?php if (!empty($availableVegetables)) { ?>

        <?= $form->field($model, 'vegetables')->widget(MultiSelect::class, [
            'data' => $availableVegetables,
            'options' => ['multiple' => 'multiple'],
        ]) ?>

    <?php } ?>
  <div class="form-group">
      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
