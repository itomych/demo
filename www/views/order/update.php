<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = 'Update Order: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-update">
    <?php $form = ActiveForm::begin(['action' => ['/order/update', 'id' => $model->id]]); ?>

  <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact(
        'model',
        'form',
        'availableBreads',
        'availableTastes',
        'availableVegetables',
        'availableSauces',
        'availableBake',
        'availableExtras',
        'availableSizes',
        'yesNo'
    )) ?>

</div>
