<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = 'Create Order';
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">
    <?php $form = ActiveForm::begin(['action' => '/order/create']); ?>

  <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact(
        'model',
        'form',
        'availableBreads',
        'availableTastes',
        'availableVegetables',
        'availableSauces',
        'availableBake',
        'availableExtras',
        'availableSizes',
        'yesNo'
    )) ?>

</div>
