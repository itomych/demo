<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
      <?= Html::a('Create Order', ['create-page'], ['class' => 'btn btn-success']) ?>
  </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'label' => 'User name',
                'value' => function ($data) {
                    return $data->user->name;
                },
            ],
            [
                'attribute' => 'meal_id',
                'label' => 'Meal name',
                'value' => function ($data) {
                    return $data->meal->name;
                },
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function ($data) {
                    return \app\models\Enums\OpenedClosed::getName($data->status);
                },
            ],
            'created_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {copy}',
                'buttons' => [
                    'update' => function ($url, $data) {
                        $url = \yii\helpers\Url::toRoute(['order/update-page', 'id' => $data->id], true);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                    },
                    'copy' => function ($url, $data, $key) {
                        $url = \yii\helpers\Url::toRoute(['order/create-page', 'id' => $data->id], true);
                        return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url);
                    },
                ],
            ],
            [
                'label' => 'Open/Close',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(
                        $data->status === 1 ? 'Close' : 'Open',
                        ['/order/' . ($data->status === 1 ? 'close' : 'open'), 'id' => $data->id],
                        [
                            'title' => 'Open',
                        ]
                    );
                },
            ],
            [
                'label' => 'Rate',
                'format' => 'raw',
                'value' => function ($data) {
                    $html = '';
                    for ($i = 1; $i <= 5; $i++) {
                        $glyphiconStar = 'glyphicon-star-empty';
                        if ($i <= $data->rate) {
                            $glyphiconStar = 'glyphicon-star';
                        }

                        $url = \yii\helpers\Url::toRoute(['order/rate', 'id' => $data->id, 'rate' => $i], true);
                        $html .= Html::a("<span class=\"glyphicon $glyphiconStar\"></span>", $url);
                    }
                    return $html;
                },
            ],
        ],
    ]); ?>

</div>
