<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Meal */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Meals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="meal-view">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
      <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Delete', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
              'confirm' => 'Are you sure you want to delete this item?',
              'method' => 'post',
          ],
      ]) ?>
  </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function ($data) {
                    return \app\models\Enums\OpenedClosed::getName($data->status);
                },
            ],
            [
                'attribute' => 'available_bake',
                'label' => 'Available bake',
                'value' => function ($data) {
                    return \app\models\Enums\YesNo::getName($data->available_bake);
                },
            ],
            [
                'attribute' => 'available_extras',
                'label' => 'Available extras',
                'value' => function (\app\models\Meal $data) {
                    $extrasNames = \app\models\Enums\Extras::getListFromValues($data->available_extras);
                    return join(', ', $extrasNames);
                },
            ],
            [
                'attribute' => 'available_sizes',
                'label' => 'Available sizes',
                'value' => function (\app\models\Meal $data) {
                    $sizesNames = \app\models\Enums\Sizes::getListFromValues($data->available_sizes);
                    return join(', ', $sizesNames);
                },
            ],
            [
                'attribute' => 'breads',
                'label' => 'Available breads',
                'value' => function (\app\models\Meal $data) {
                    return \app\helpers\StringHelper::getJoinedNames($data->breads);
                },
            ],
            [
                'attribute' => 'tastes',
                'label' => 'Available tastes',
                'value' => function (\app\models\Meal $data) {
                    return \app\helpers\StringHelper::getJoinedNames($data->tastes);
                },
            ],
            [
                'attribute' => 'sauces',
                'label' => 'Available sauces',
                'value' => function (\app\models\Meal $data) {
                    return \app\helpers\StringHelper::getJoinedNames($data->sauces);
                },
            ],
            [
                'attribute' => 'vegetables',
                'label' => 'Available vegetables',
                'value' => function (\app\models\Meal $data) {
                    return \app\helpers\StringHelper::getJoinedNames($data->vegetables);
                },
            ],
            [
                'attribute' => 'access_code',
                'label' => 'Access link',
                'value' => function ($data) {
                    return \yii\helpers\Url::toRoute(
                        ['meal/view', 'id' => $data->id, 'code' => $data->access_code]
                        , true
                    );
                },
            ],
            'created_at',
        ],
    ]) ?>

</div>
