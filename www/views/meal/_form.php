<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Meal */
/* @var $form yii\widgets\ActiveForm */
/* @var $openClose array */
/* @var $yesNo array */
/* @var $extras array */
/* @var $sizes array */
/* @var $breads app\models\Bread[] */
/* @var $tastes app\models\Taste[] */
/* @var $sauces app\models\Sauce[] */
/* @var $vegetables app\models\Vegetable[] */
//var_dump($model->status);
//die;
?>

<div class="meal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($openClose) ?>

    <?= $form->field($model, 'available_bake')->dropDownList($yesNo) ?>

    <?= $form->field($model, 'available_extras')->checkboxList($extras) ?>

    <?= $form->field($model, 'available_sizes')->checkboxList($sizes) ?>

    <?= $form->field($model, 'breads')->checkboxList($breads) ?>

    <?= $form->field($model, 'tastes')->checkboxList($tastes) ?>

    <?= $form->field($model, 'sauces')->checkboxList($sauces) ?>

    <?= $form->field($model, 'vegetables')->checkboxList($vegetables) ?>

  <div class="form-group">
      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
