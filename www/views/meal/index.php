<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Meals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meal-index">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
      <?= Html::a('Create Meal', ['create'], ['class' => 'btn btn-success']) ?>
  </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function ($data) {
                    return \app\models\Enums\OpenedClosed::getName($data->status);
                },
            ],
            [
                'attribute' => 'access_code',
                'label' => 'Access link',
                'value' => function ($data) {
                    return \yii\helpers\Url::toRoute(
                        ['meal/view', 'id' => $data->id, 'code' => $data->access_code]
                        , true
                    );
                },
            ],
            'created_at',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
