<?php

namespace app\commands;

use app\rbac\UserGroupRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    /**
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $createPost = $auth->createPermission('createOrder');
        $auth->add($createPost);

        $adminChanges = $auth->createPermission('adminChanges');
        $auth->add($adminChanges);

        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createPost);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $adminChanges);

        $rule = new \app\rbac\AuthorRule;
        $auth->add($rule);

        $updateOwnOrder = $auth->createPermission('updateOwnOrder');
        $updateOwnOrder->ruleName = $rule->name;
        $auth->add($updateOwnOrder);

        $auth->addChild($author, $updateOwnOrder);

        $auth->assign($admin, 'admin');
    }
}