<?php

namespace app\helpers;

class StringHelper
{
    /**
     * @param array $data
     * @return string
     */
    public static function getJoinedNames(array $data): string
    {
        $names = array_column($data, 'name');

        return join(', ', $names);
    }
}