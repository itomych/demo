<?php

namespace app\helpers;

use Yii;
use yii\helpers\Html;

class NavHelper
{
    /**
     * @return array
     */
    public static function getMenu()
    {
        $menu = [
            ['label' => 'Home', 'url' => ['/site/index']],
        ];

        $userId = Yii::$app->user->getId();

        if ($userId) {
            $menu[] = ['label' => 'Order', 'url' => '/order'];

            $isAdmin = Yii::$app->authManager->getAssignment('admin', $userId);
            if ($isAdmin) {
                $menu = array_merge($menu, [
                    ['label' => 'Meal', 'url' => '/meal'],
                    ['label' => 'User', 'url' => '/user'],
                    ['label' => 'Bread', 'url' => '/bread'],
                    ['label' => 'Taste', 'url' => '/taste'],
                    ['label' => 'Sauce', 'url' => '/sauce'],
                    ['label' => 'Vegetable', 'url' => '/vegetable'],
                ]);
            }
        }

        $menu[] = static::getLogoutOrLoginButton();

        return $menu;
    }

    public static function getLogoutOrLoginButton()
    {
        if (Yii::$app->user->isGuest) {
            return ['label' => 'Login', 'url' => ['/site/login']];
        }

        return
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->name . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
}