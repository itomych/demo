<?php

namespace app\auth;

use app\models\User as UserIdentity;
use yii\filters\auth\AuthMethod;
use yii\web\IdentityInterface;
use yii\web\Request;
use yii\web\Response;
use yii\web\User;

class AuthLink extends AuthMethod
{

    /**
     * Authenticates the current user.
     * @param User $user
     * @param Request $request
     * @param Response $response
     * @return IdentityInterface the authenticated user identity. If authentication information is not provided, null will be returned.
     * @throws \Throwable
     */
    public function authenticate($user, $request, $response)
    {

        if ($user->getIdentity() && $user->getIdentity()->role === 'admin') {
            return $user->getIdentity();
        }
        $code = $request->get('code');
        if (empty($code)) {
            return null;
        }
        return UserIdentity::findOne(['unique_code' => $code]);
    }
}