<?php

namespace app\services;

use app\models\Enums\Extras;
use app\models\Enums\Sizes;
use app\models\Enums\YesNo;
use app\models\Meal;
use app\models\Order;
use app\models\Vegetable;

class OrderService
{
    /**
     * @param Meal $meal
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getAvailableChoices(Meal $meal): array
    {
        $availableBreads = array_column($meal->getBreads()->all(), 'name', 'id');
        $availableTastes = array_column($meal->getTastes()->all(), 'name', 'id');
        $availableVegetables = array_column($meal->getVegetables()->all(), 'name', 'id');
        $availableSauces = array_column($meal->getSauces()->all(), 'name', 'id');
        $availableExtras = Extras::getListFromValues($meal->available_extras);
        $availableSizes = Sizes::getListFromValues($meal->available_sizes);
        $availableBake = $meal->available_bake;
        $yesNo = YesNo::getList();

        return compact(
            'availableBreads',
            'availableTastes',
            'availableVegetables',
            'availableSauces',
            'availableBake',
            'availableExtras',
            'availableSizes',
            'yesNo'
        );
    }

    /**
     * @param Order $order
     * @param mixed $vegetables
     */
    public function linkVegetables(Order $order, $vegetables): void
    {
        $vegetables = empty($vegetables) ? [] : $vegetables;

        foreach ($vegetables as $vegetable) {
            $vegetable = Vegetable::findOne(['id' => $vegetable]);
            $order->link('vegetables', $vegetable);
        }

        $order->save();
    }

}