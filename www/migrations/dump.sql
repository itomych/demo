-- MySQL dump 10.13  Distrib 8.0.15, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: subway
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bread`
--

DROP TABLE IF EXISTS `bread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bread`
--

LOCK TABLES `bread` WRITE;
/*!40000 ALTER TABLE `bread` DISABLE KEYS */;
INSERT INTO `bread` VALUES (2,'Bread1'),(3,'Bread2');
/*!40000 ALTER TABLE `bread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal`
--

DROP TABLE IF EXISTS `meal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  `available_bake` tinyint(1) NOT NULL,
  `available_extras` set('bacon','meat','cheese') COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_sizes` set('15','30') COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_code_UNIQUE` (`access_code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal`
--

LOCK TABLES `meal` WRITE;
/*!40000 ALTER TABLE `meal` DISABLE KEYS */;
INSERT INTO `meal` VALUES (9,'Meal1','2019-03-28 15:04:00',1,1,'bacon,meat','15,30','5c9ce26062ab1');
/*!40000 ALTER TABLE `meal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal_bread_available`
--

DROP TABLE IF EXISTS `meal_bread_available`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meal_bread_available` (
  `meal_id` int(11) NOT NULL,
  `bread_id` int(11) NOT NULL,
  PRIMARY KEY (`meal_id`,`bread_id`),
  KEY `meal_bread_bread_foreign_idx` (`bread_id`),
  CONSTRAINT `meal_bread_bread_foreign` FOREIGN KEY (`bread_id`) REFERENCES `bread` (`id`) ON DELETE CASCADE,
  CONSTRAINT `meal_bread_meal_foreign` FOREIGN KEY (`meal_id`) REFERENCES `meal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal_bread_available`
--

LOCK TABLES `meal_bread_available` WRITE;
/*!40000 ALTER TABLE `meal_bread_available` DISABLE KEYS */;
INSERT INTO `meal_bread_available` VALUES (9,2),(9,3);
/*!40000 ALTER TABLE `meal_bread_available` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal_sauce_available`
--

DROP TABLE IF EXISTS `meal_sauce_available`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meal_sauce_available` (
  `meal_id` int(11) NOT NULL,
  `sauce_id` int(11) NOT NULL,
  PRIMARY KEY (`meal_id`,`sauce_id`),
  KEY `souce_available_souce_foreign_idx` (`sauce_id`),
  CONSTRAINT `souce_available_meal_foreign` FOREIGN KEY (`meal_id`) REFERENCES `meal` (`id`) ON DELETE CASCADE,
  CONSTRAINT `souce_available_sauce_foreign` FOREIGN KEY (`sauce_id`) REFERENCES `sauce` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal_sauce_available`
--

LOCK TABLES `meal_sauce_available` WRITE;
/*!40000 ALTER TABLE `meal_sauce_available` DISABLE KEYS */;
INSERT INTO `meal_sauce_available` VALUES (9,1),(9,2);
/*!40000 ALTER TABLE `meal_sauce_available` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal_taste_available`
--

DROP TABLE IF EXISTS `meal_taste_available`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meal_taste_available` (
  `meal_id` int(11) NOT NULL,
  `taste_id` int(11) NOT NULL,
  PRIMARY KEY (`meal_id`,`taste_id`),
  KEY `taste_idx` (`taste_id`),
  CONSTRAINT `taste_abailable_taste_foreign` FOREIGN KEY (`taste_id`) REFERENCES `taste` (`id`) ON DELETE CASCADE,
  CONSTRAINT `taste_available_meal_foreign` FOREIGN KEY (`meal_id`) REFERENCES `meal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal_taste_available`
--

LOCK TABLES `meal_taste_available` WRITE;
/*!40000 ALTER TABLE `meal_taste_available` DISABLE KEYS */;
INSERT INTO `meal_taste_available` VALUES (9,1),(9,2);
/*!40000 ALTER TABLE `meal_taste_available` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal_vegetable_available`
--

DROP TABLE IF EXISTS `meal_vegetable_available`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meal_vegetable_available` (
  `meal_id` int(11) NOT NULL,
  `vegetable_id` int(11) NOT NULL,
  PRIMARY KEY (`meal_id`,`vegetable_id`),
  KEY `vegetable_foreign_idx` (`vegetable_id`),
  CONSTRAINT `vegetable_available_meal_foreign` FOREIGN KEY (`meal_id`) REFERENCES `meal` (`id`) ON DELETE CASCADE,
  CONSTRAINT `vegetable_available_vegetable_foreign` FOREIGN KEY (`vegetable_id`) REFERENCES `vegetable` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal_vegetable_available`
--

LOCK TABLES `meal_vegetable_available` WRITE;
/*!40000 ALTER TABLE `meal_vegetable_available` DISABLE KEYS */;
INSERT INTO `meal_vegetable_available` VALUES (9,1),(9,2);
/*!40000 ALTER TABLE `meal_vegetable_available` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `meal_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `bread_id` int(11) DEFAULT NULL,
  `size` enum('15','30') COLLATE utf8mb4_unicode_ci NOT NULL,
  `should_baked` tinyint(1) NOT NULL,
  `taste_id` int(11) DEFAULT NULL,
  `sauce_id` int(11) DEFAULT NULL,
  `extras` set('bacon','meat','cheese') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` smallint(3) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_meal_foreign_idx` (`bread_id`),
  KEY `order_taste_foreign_idx` (`taste_id`),
  KEY `order_souce_id_idx` (`sauce_id`),
  KEY `order_user_foreign_idx` (`user_id`),
  KEY `order_meal_id_idx` (`meal_id`),
  CONSTRAINT `order_bread_foreign` FOREIGN KEY (`bread_id`) REFERENCES `bread` (`id`) ON DELETE SET NULL,
  CONSTRAINT `order_meal_foreign` FOREIGN KEY (`meal_id`) REFERENCES `meal` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_souce_foreign` FOREIGN KEY (`sauce_id`) REFERENCES `sauce` (`id`) ON DELETE SET NULL,
  CONSTRAINT `order_taste_foreign` FOREIGN KEY (`taste_id`) REFERENCES `taste` (`id`) ON DELETE SET NULL,
  CONSTRAINT `order_user_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (24,6,9,1,2,'30',0,1,2,'bacon',3,'2019-03-28 15:04:41'),(25,6,9,1,2,'30',0,1,2,'bacon',3,'2019-03-28 15:04:59');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_vegetable`
--

DROP TABLE IF EXISTS `order_vegetable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `order_vegetable` (
  `order_id` int(11) NOT NULL,
  `vegetable_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`,`vegetable_id`),
  KEY `order_vegetable_vegetable_foreign_idx` (`vegetable_id`),
  CONSTRAINT `order_vegetable_order_foreign` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_vegetable_vegetable_foreign` FOREIGN KEY (`vegetable_id`) REFERENCES `vegetable` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_vegetable`
--

LOCK TABLES `order_vegetable` WRITE;
/*!40000 ALTER TABLE `order_vegetable` DISABLE KEYS */;
INSERT INTO `order_vegetable` VALUES (25,1),(24,2),(25,2);
/*!40000 ALTER TABLE `order_vegetable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sauce`
--

DROP TABLE IF EXISTS `sauce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sauce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sauce`
--

LOCK TABLES `sauce` WRITE;
/*!40000 ALTER TABLE `sauce` DISABLE KEYS */;
INSERT INTO `sauce` VALUES (1,'Sauce1'),(2,'Sauce2');
/*!40000 ALTER TABLE `sauce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taste`
--

DROP TABLE IF EXISTS `taste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `taste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taste`
--

LOCK TABLES `taste` WRITE;
/*!40000 ALTER TABLE `taste` DISABLE KEYS */;
INSERT INTO `taste` VALUES (1,'Taste1'),(2,'Taste2');
/*!40000 ALTER TABLE `taste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unique_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code_UNIQUE` (`unique_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (6,'Petya','5c9cbd299ce10');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vegetable`
--

DROP TABLE IF EXISTS `vegetable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `vegetable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vegetable`
--

LOCK TABLES `vegetable` WRITE;
/*!40000 ALTER TABLE `vegetable` DISABLE KEYS */;
INSERT INTO `vegetable` VALUES (1,'Vegetable1'),(2,'Vegetable2');
/*!40000 ALTER TABLE `vegetable` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-28 17:05:27
