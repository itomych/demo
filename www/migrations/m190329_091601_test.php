<?php

use yii\db\Migration;

/**
 * Class m190329_091601_test
 */
class m190329_091601_test extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ . '/dump.sql'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190329_091601_test cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190329_091601_test cannot be reverted.\n";

        return false;
    }
    */
}
