# Subway

#####`docker` and `docker-compose` must be installed in the system

 Docker - https://docs.docker.com/install/
 
 Docker compose - https://docs.docker.com/compose/install/
 
##

```unzip subway.zip```

> Go to **subway** folder, and start 

$ ```docker-composer up```
> Waiting few seconds for starting services (usually 30 seconds) 

> Run command for install dependencies

$ ```docker-compose exec subwayphp php /var/www/composer.phar install -d /var/www -n```

> Run command for fill database

$ ```docker-compose exec subwayphp /var/www/yii migrate --interactive=0```

> After execute, go to http://localhost

> Login for admin: **admin / admin**
